<?php
require "bootstrap.php";
use Chatter\Models\User;
use Chatter\Models\Product;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());



//Q2
$app->get('/products', function($request, $response,$args){
    $_product = new Product();
    $products = $_product->all();

    $payload = [];
    foreach($products as $u){
        $payload[$u->id] = [
            "name" => $u->name,
            "price" => $u->price
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//Q3
$app->post('/products/search', function($request, $response, $args){    
    $name= $request->getParsedBodyParam('name',''); 
    $products= Product::where('name', '=', $name)->get(); 
    $payload=[];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'id'=> $prd->id,
            'name'=> $prd->name,
            'price'=> $prd->price
        ];
    }

    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});



$app->put('/products/{product_id}', function($request, $response, $args){
   $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = Product::find($args['product_id']);
    $_product->name = $name;
    $_product->price = $price;
  
    if($_product->save()){
        $payload = ['product_id' => $_product->id,"result" => "The product has been updates successfuly"];
       return $response->withStatus(200)->withJson($payload);
   }
    else{
        return $response->withStatus(400);
    }
});

$app->get('/products/{product_id}', function($request, $response,$args){
    $_id = $args['product_id'];
   $product = Product::find($_id);
    return $response->withStatus(200)->withJson($product);
});



$app->post('/products', function($request, $response, $args){
    $name  = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');    
    $_product = new Product();
    $_product->name=$name;
    $_product->price =$price;
    $_product->save();
    if($_product->id){
        $payload = ['product_id'=>$_product->id];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});




$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();